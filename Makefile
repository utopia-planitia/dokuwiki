
include ../kubernetes/etc/help.mk
include ../kubernetes/etc/cli.mk

.PHONY: deploy
deploy: ##@setup send setup definition to kubernetes apiserver
	$(CLI) kubectl apply -f namespace.yaml -f dokuwiki.yaml -f backup.yaml

.PHONY: proxy
proxy: ##@setup proxy apiserver to http://127.0.0.1:8001/
	@echo Dokuwiki: http://localhost:8001/api/v1/namespaces/dokuwiki/services/dokuwiki/proxy/
	$(CLI) kubectl proxy
