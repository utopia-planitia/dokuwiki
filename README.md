# Dokuwiki

A deployment of Dokuwiki
https://www.dokuwiki.org/dokuwiki#

## Usage

run `make` for usage

## Access

run `make proxy` to forward apiserver to port `http://127.0.0.1:8001`

Dokuwiki: http://localhost:8001/api/v1/namespaces/dokuwiki/services/dokuwiki/proxy/
